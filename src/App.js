import { MemoryRouter as Router, Routes, Route } from "react-router-dom";
import HomePage from "./pages/index";

import Layout from "./components/Layout";
import RequirementsPage from "./pages/requirements";
import ResearchPage from "./pages/research";
import HCIPage from "./pages/hci";
import DesignPage from "./pages/design";
import TestingPage from "./pages/testing";
import EvaluationPage from "./pages/evaluation";
import AppendicesPage from "./pages/appendices";
import ErrorPage from "./pages/404";

function App() {
  return (
    <Router>
      <Layout>
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/requirements" element={<RequirementsPage />} />
          <Route path="/research" element={<ResearchPage />} />
          <Route path="/hci" element={<HCIPage />} />
          <Route path="/design" element={<DesignPage />} />
          <Route path="/testing" element={<TestingPage />} />
          <Route path="/evaluation" element={<EvaluationPage />} />
          <Route path="/appendices" element={<AppendicesPage />} />
          <Route path="*" element={<ErrorPage />} />
        </Routes>
      </Layout>
    </Router>
  );
}
export default App;
