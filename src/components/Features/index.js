import React from "react";
import tw from "twin.macro";

import {
  AimOutlined,
  CopyFilled,
  EditFilled,
  EyeFilled,
} from "@ant-design/icons";
import Section from "../Section";

// const Container = tw.div`
// container mx-auto flex flex-wrap items-center justify-between pb-12
// `;

// const Title = tw.h2`
// w-full my-2 text-xl font-black leading-tight text-center text-gray-800 lg:mt-8
// `;

const FeaturesContainer = tw.div`
grid grid-rows-4 md:grid-cols-4 md:grid-rows-1 justify-center text-xl text-gray-500 font-bold opacity-75
`;

const index = () => {
  return (
    <Section title="Features">
      <div className="w-full mb-4 h-1 mx-auto gradient w-64 opacity-25 my-0 py-0 rounded-t"></div>
      <FeaturesContainer>
        <span className="p-4 md:w-auto flex items-center justify-center">
          <div className="flex flex-row space-x-4 justify-center items-center">
            <EyeFilled style={{ color: "#9CA3AF", fontSize: 40 }} />
            <div>Visual</div>
          </div>
        </span>

        <span className="p-4 md:w-auto flex items-center justify-center">
          <div className="flex flex-row space-x-4 justify-center items-center">
            <AimOutlined style={{ color: "#9CA3AF", fontSize: 40 }} />
            <div>Drill Down</div>
          </div>
        </span>

        <span className="p-4 md:w-auto flex items-center justify-center">
          <div className="flex flex-row space-x-4 justify-center items-center">
            <EditFilled style={{ color: "#9CA3AF", fontSize: 40 }} />
            <div>Annotation</div>
          </div>
        </span>

        <span className="p-4 md:w-auto flex items-center justify-center">
          <div className="flex flex-row space-x-4 justify-center items-center">
            <CopyFilled style={{ color: "#9CA3AF", fontSize: 40 }} />
            <div>Generate Report</div>
          </div>
        </span>
      </FeaturesContainer>
    </Section>
  );
};

export default index;
