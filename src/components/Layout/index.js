import React from "react";
import { GlobalStyles } from "twin.macro";
import Nav from "../Nav";

const links = [
  "Blog",
  "Requirements",
  "Research",
  "HCI",
  "Design",
  "Testing",
  "Evaluation",
  "Appendices",
];

const index = ({ children }) => {
  return (
    <>
      <GlobalStyles />
      <div style={{ backgroundColor: "#121B3A" }}>
        <Nav links={links} />
        {children}
      </div>
    </>
  );
};

export default index;
