import React, { useState } from "react";
import tw, { styled } from "twin.macro";

const SectionTitle = styled.h1`
  ${tw`font-bold text-lg cursor-pointer text-center`}
  ${(props) => (props.underline ? tw`underline` : ``)}
`;

const ListContainer = tw.ul`list-decimal space-y-4 my-8`;

const List = ({ data }) => {
  return (
    <ListContainer>
      {data.map((info) => (
        <li>{info}</li>
      ))}
    </ListContainer>
  );
};

const Index = ({ data, defaultOption, list = false }) => {
  const [selected, setSelected] = useState(defaultOption);

  return (
    <div className="mx-auto">
      <div className="flex flex-row space-x-12 justify-center">
        {Object.keys(data).map((option) => (
          <SectionTitle
            underline={selected === option}
            onClick={() => setSelected(option)}
          >
            {option}
          </SectionTitle>
        ))}
      </div>

      {!list ? (
        data[selected].map((item) => {
          return (
            <div className="space-y-4 my-6">
              <h1 className="font-bold">{item.key}</h1>
              <p>{item.value}</p>
            </div>
          );
        })
      ) : (
        <List data={data[selected]} />
      )}
    </div>
  );
};

export default Index;
