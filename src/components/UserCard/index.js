import React from "react";
import { LinkedinFilled, MailFilled, GithubFilled } from "@ant-design/icons";
import tw, { styled } from "twin.macro";

const Container = styled.div`
  ${tw`w-full p-2 flex flex-col flex-grow flex-shrink shadow-lg rounded`}
`;

const ActionButton = tw.div`lg:mx-0 hover:underline text-gray-800 font-extrabold rounded my-2 py-2 px-6 shadow-lg rounded cursor-pointer

`;

const UserCard = ({ name, email, content }) => {
  return (
    <Container>
      <div className="flex-1 bg-white rounded-t rounded-b-none overflow-hidden">
        <div className="flex flex-wrap no-underline hover:no-underline">
          <p className="w-full text-gray-600 font-bold text-lg px-4 mt-6">
            {name}
          </p>
          <p className="text-gray-600 text-base px-4 mb-5 my-4">{content}</p>
        </div>
      </div>

      <div className="flex flex-row justify-center md:w-full md:justify-center space-x-5">
        <ActionButton>
          <LinkedinFilled />
        </ActionButton>
        <ActionButton>
          <GithubFilled />
        </ActionButton>

        <ActionButton>
          {/* <a href={`mailto:${email}`}> */}
          <MailFilled />
          {/* </a> */}
        </ActionButton>
      </div>
    </Container>
  );
};

export default UserCard;
