import React from "react";
import Layout from "../components/Layout";
import PageTitle from "../components/PageTitle";
import Section from "../components/Section";

const Blog = () => {
  return (
    <Layout>
      <PageTitle title="Blog" />
      <Section title="Content Coming soon...."></Section>
    </Layout>
  );
};

export default Blog;
