import React from "react";
import Section from "../components/Section";
import ListImages from "../components/ListImages";

import PageTitle from "../components/PageTitle";

import { sketches, figmas } from "../data/hci/data";

const HCI = () => {
  return (
    <>
      <PageTitle title="Human Computer Interaction" />

      <Section title="Sketches" subtitle="First Iteration">
        <ListImages data={sketches} />
      </Section>

      <Section title="Figma" subtitle="Final Design" shade>
        <ListImages data={figmas} />
      </Section>
    </>
  );
};

export default HCI;
