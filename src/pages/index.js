import * as React from "react";
import Header from "../components/header";
import UserCard from "../components/UserCard";
import Features from "../components/Features";
import LandingImage from "../images/index/landing.png";
import Section from "../components/Section";
import { Problems } from "../data/landing";
import CardWithIcon from "../components/CardWithIcon";
import tw, { styled } from "twin.macro";

const MockUpContainer = styled.div`
  ${tw`flex flex-col md:flex-row md:items-center md:justify-center mx-12 md:px-24`}
`;

const IndexPage = () => {
  return (
    <>
      <section className="border-b pt-12 md:pb-12 text-black">
        <MockUpContainer>
          <div className="w-full md:w-7/12">
            <Header
              title="Application to Analyse Gene Clusters"
              subtitles={[
                "Team 27 - UCL Computer Science",
                "Zain Saleem - Ammaar Bin-Maajid - Passawis Chaiyapattanaporn",
              ]}
            />
          </div>
          <div className="w-full md:w-5/12">
            <img
              src={LandingImage}
              width="800"
              height="600"
              alt="laptop with NTT Analysis software shown on screen"
            />
          </div>
        </MockUpContainer>
      </section>
      <Section title="Abstract" shade>
        <p>
          NTT DATA is a Japanese multinational information technology service
          and consulting company. They help clients transform through
          consulting, industry solutions, business process services, IT
          modernization and managed services. In this project, we worked
          alongside the executives at NTT DATA to deliver a web application
          purposed for providing graphical information on the similarities and
          differences between different gene clusters.
        </p>
        <p className="mt-12">
          The Gene Clusters Analysis platform (GCA) will significantly assist
          the process in which researcher’s extract relevant gene cluster
          information, whilst preserving a great level of simplicity.
        </p>
      </Section>
      <Section title="The Problem">
        <h3 className="text-xl text-black text-center">
          Visual analysis of common gene clusters in disease causing viruses in
          the Coronaviridae category
        </h3>
        <div className="w-full mb-4">
          <div className="h-1 mx-auto gradient w-64 opacity-25 my-0 py-0 rounded-t"></div>
        </div>

        <div className="flex flex-col items-center space-y-8 lg:flex-row lg:flex-wrap lg:justify-start lg:space-y-0 lg:space-x-6">
          {Problems.map((data) => (
            <CardWithIcon title={data.title} content={data.content} />
          ))}
        </div>
      </Section>

      <Features />

      <Section title="Meet our Team">
        <div className="grid grid-rows-3 md:grid-cols-3 md:grid-rows-1 gap-4">
          <UserCard
            name={"Ammaar Bin-Maajid"}
            email="zcabbin@ucl.ac.uk"
            content="Ammaar is a second year computer science student at UCL who has a passion for cyber security, data analysis and data science. "
          />

          <UserCard
            name={"Passawis Chaiyapattanaporn"}
            email="zcabpc0@ucl.ac.uk"
            content="Passawis is pursuing a MEng in Computer Science at UCL and has great passion for data science and finance. He is eager to learn new technologies and tools and utilising to tackle problems and projects."
          />

          <UserCard
            name={"Zain Saleem"}
            email="zain.saleem@ucl.ac.uk"
            content="Zain is currently pursuing a BSc in Computer Science at University College London (2nd year) and has a great passion for application development and data science. He is always always keen to learn new technologies and work on exciting, challenging projects."
          />
        </div>
      </Section>
    </>
  );
};

export default IndexPage;
