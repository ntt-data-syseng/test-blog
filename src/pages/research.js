import React from "react";
import Section from "../components/Section";
import PageTitle from "../components/PageTitle";

const Research = () => {
  return (
    <>
      <PageTitle title="Research" />
      <Section title="Neo4J Overview" shade>
        <h1>Test</h1>
      </Section>
      <Section title="Tech Stack">
        <h1>Talk about react graphql postgres etc.... redis</h1>
      </Section>
      <Section title="References" shade>
        <h1>Reference youtube video etc libraries</h1>
      </Section>
    </>
  );
};

export default Research;
