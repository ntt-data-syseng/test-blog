import React from "react";

import PageTitle from "../components/PageTitle";
import Section from "../components/Section";

const Testing = () => {
  return (
    <>
      <PageTitle title="Testing" />
      <Section title="Content Coming soon...."></Section>
    </>
  );
};

export default Testing;
